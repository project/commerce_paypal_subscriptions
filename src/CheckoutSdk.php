<?php

namespace Drupal\commerce_paypal_subscriptions;

use Drupal\commerce_paypal\CheckoutSdk as OriginalCheckoutSdk;

/**
 * Checkout API extended with specific subscription methods.
 */
class CheckoutSdk extends OriginalCheckoutSdk {

  /**
   * Gets a subscription by its ID.
   *
   * @param string $subscription_id
   *   Subscription ID.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Response that contains the subscription, if exists.
   */
  public function getSubscription(string $subscription_id) {
    return $this->client->get(sprintf('/v1/billing/subscriptions/%s', $subscription_id));
  }

  /**
   * Creates a Paypal product.
   *
   * Creating products allows creating plans that refers to a specific product.
   *
   * @param string $name
   *   Product name.
   * @param string $description
   *   Product description.
   * @param string $type
   *   Product type.
   * @param string $category
   *   Product category.
   * @param string|null $image_url
   *   Product image URL.
   * @param string|null $home_url
   *   Product Home URL.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Response interface.
   */
  public function createProduct(string $name, string $description, string $type, string $category, string $image_url = NULL, string $home_url = NULL) {
    return $this->client->post('/v1/catalogs/products', [
      'json' => [
        'name' => $name,
        'description' => $description,
        'type' => $type,
        'category' => $category,
        'image_url' => $image_url,
        'home_url' => $home_url,
      ],
    ]);
  }

  /**
   * Creates a subscription plan.
   *
   * @param string $product_id
   *   Product ID.
   * @param string $name
   *   Product name.
   * @param string $description
   *   Description.
   * @param array $billing_cycles
   *   Billing cycles.
   * @param array $payment_preferences
   *   Payment preferences.
   * @param array $taxes
   *   Taxes.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Response.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function createSubscriptionPlan(string $product_id, string $name, string $description, array $billing_cycles, array $payment_preferences = [], array $taxes = []) {
    $data = [
      'product_id' => $product_id,
      'name' => $name,
      'description' => $description,
      'billing_cycles' => $billing_cycles,
      'payment_preferences' => (object) $payment_preferences,
    ];
    if (!empty($taxes)) {
      $data['taxes'] = (object) $taxes;
    }
    return $this->client->post('/v1/billing/plans', [
      'json' => $data,
    ]);
  }

  /**
   * Gets the list of products.
   */
  public function getProducts() {
    return $this->client->get('/v1/catalogs/products');
  }

}
