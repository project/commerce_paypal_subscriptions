<?php

namespace Drupal\commerce_paypal_subscriptions;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\commerce_paypal\Plugin\Commerce\PaymentGateway\CheckoutInterface;
use Drupal\commerce_paypal\SmartPaymentButtonsBuilderInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Url;

/**
 * Builds payment buttons for paypal subscriptions.
 */
class SmartPaymentsButtonsBuilder implements SmartPaymentButtonsBuilderInterface {

  /**
   * {@inheritdoc}
   */
  public function build(OrderInterface $order, PaymentGatewayInterface $payment_gateway, $commit) {
    $element = [];
    if (!$payment_gateway->getPlugin() instanceof CheckoutInterface) {
      return $element;
    }

    $config = $payment_gateway->getPlugin()->getConfiguration();

    $create_url = Url::fromRoute('commerce_paypal_subscriptions.checkout.create', [
      'commerce_order' => $order->id(),
      'commerce_payment_gateway' => $payment_gateway->id(),
    ]);

    $return_url = Url::fromRoute('commerce_paypal_subscriptions.checkout.approve', [
      'commerce_order' => $order->id(),
      'commerce_payment_gateway' => $payment_gateway->id(),
    ]);

    $cancel_url = Url::fromRoute('commerce_payment.checkout.cancel', [
      'commerce_order' => $order->id(),
      'step' => 'payment',
    ]);

    /** @var \Drupal\commerce_price\Price $total_price */
    $total_price = $order->getTotalPrice();
    $options = [
      'query' => [
        'client-id' => $config['client_id'],
        'intent' => 'subscription',
        'vault' => 'true',
        'currency' => $total_price->getCurrencyCode(),
      ],
    ];

    // Enable Venmo funding if it is not disabled.
    if (($key = array_search('venmo', $config['disable_funding'])) === FALSE) {
      $options['query']['enable-funding'] = 'venmo';
      unset($config['disable_funding'][$key]);
    }
    if (!empty($config['disable_funding'])) {
      $options['query']['disable-funding'] = implode(',', $config['disable_funding']);
    }
    if (!empty($config['disable_card'])) {
      $options['query']['disable-card'] = implode(',', $config['disable_card']);
    }

    $element['#attached']['library'][] = 'commerce_paypal_subscriptions/paypal_checkout';
    $element_id = Html::getUniqueId('paypal-buttons-container');
    $element['#attached']['drupalSettings']['paypalSubscriptions'][$order->id()] = [
      'src' => Url::fromUri('https://www.paypal.com/sdk/js', $options)->toString(),
      'elementId' => $element_id,
      'onCreateUrl' => $create_url->toString(),
      'onApproveUrl' => $return_url->toString(),
      'onCancelUrl' => $cancel_url->toString(),
      'style' => $config['style'],
    ];
    $element += [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#weight' => 100,
      '#attributes' => [
        'class' => ['paypal-buttons-container'],
        'id' => $element_id,
      ],
    ];

    return $element;
  }

}
