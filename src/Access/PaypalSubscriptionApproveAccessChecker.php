<?php

declare(strict_types=1);

namespace Drupal\commerce_paypal_subscriptions\Access;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Checks if passed parameter matches the route configuration.
 *
 * Usage example:
 * @code
 * foo.example:
 *   path: '/example/{parameter}'
 *   defaults:
 *     _title: 'Example'
 *     _controller: '\Drupal\commerce_paypal_subscriptions\Controller\CommercePaypalSubscriptionsController'
 *   requirements:
 *     _paypal_subscription_approve: 'some value'
 * @endcode
 */
final class PaypalSubscriptionApproveAccessChecker implements AccessInterface {

  /**
   * Access callback.
   *
   * @DCG
   * Drupal does some magic when resolving arguments for this callback. Make
   * sure the parameter name matches the name of the placeholder defined in the
   * route, and it is of the same type.
   * The following additional parameters are resolved automatically.
   *   - \Drupal\Core\Routing\RouteMatchInterface
   *   - \Drupal\Core\Session\AccountInterface
   *   - \Symfony\Component\HttpFoundation\Request
   *   - \Symfony\Component\Routing\Route
   */
  public function access(Request $request, RouteMatchInterface $routeMatch): AccessResult {
    $order = $routeMatch->getParameter('commerce_order');
    if (!$order instanceof OrderInterface) {
      return AccessResult::forbidden('Missing commerce order.');
    }

    if (!$request->request->has('orderID')) {
      return AccessResult::forbidden('Missing paypal order ID.');
    }

    if (!$request->request->has('subscriptionID')) {
      return AccessResult::forbidden('Missing paypal subscription ID.');
    }

    $subscription_id = $request->request->get('subscriptionID');

    if (!is_string($subscription_id)) {
      return AccessResult::forbidden('Invalid subscription ID.');
    }

    return AccessResult::allowed();
  }

}
