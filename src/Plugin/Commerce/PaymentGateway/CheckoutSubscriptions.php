<?php

namespace Drupal\commerce_paypal_subscriptions\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_paypal\Plugin\Commerce\PaymentGateway\Checkout;
use Drupal\commerce_paypal\Plugin\Commerce\PaymentGateway\CheckoutInterface;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the PayPal Checkout Subscriptions payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "paypal_checkout_subscriptions",
 *   label = @Translation("PayPal Checkout Subscriptions"),
 *   display_label = @Translation("PayPal Subscriptions"),
 *   modes = {
 *     "test" = @Translation("Sandbox"),
 *     "live" = @Translation("Live"),
 *   },
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_paypal_subscriptions\PluginForm\Checkout\PaymentOffsiteForm",
 *   },
 *   credit_card_types = {
 *     "amex",
 *     "dinersclub",
 *     "discover",
 *     "jcb",
 *     "maestro",
 *     "mastercard",
 *     "visa",
 *     "unionpay"
 *   },
 *   payment_method_types = {"paypal_checkout"},
 *   payment_type = "paypal_checkout",
 *   requires_billing_information = FALSE,
 * )
 *
 * @see https://developer.paypal.com/docs/subscriptions/
 */
class CheckoutSubscriptions extends Checkout implements CheckoutInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->checkoutSdkFactory = $container->get('commerce_paypal_subscriptions.checkout_sdk_factory');
    $instance->logger = $container->get('logger.channel.commerce_paypal_subscriptions');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    unset($form['intent']);

    $form['dynamic_plans'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Dynamic plans'),
      '#description' => $this->t('For each price, generates a plan at checkout. Recommended to have dynamic prices of subscriptions.'),
      '#default_value' => $this->configuration['dynamic_plans'],
    ];

    $form['default_subscription_plan'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default subscription plan'),
      '#description' => $this->t('The ID of a plan that must exists in the paypal configuration. Leave if empty if you want to dinamycally assign it.'),
      '#default_value' => $this->configuration['default_subscription_plan'],
      '#states' => [
        'visible' => [
          $this->getFieldInputSelecctor('dynamic_plans') => ['checked' => FALSE],
        ],
      ],
    ];

    $form['product_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Product ID'),
      '#description' => $this->t('The product ID of the product that will be bought through this payment method.'),
      '#default_value' => $this->configuration['product_id'],
      '#states' => [
        'visible' => [
          $this->getFieldInputSelecctor('dynamic_plans') => ['checked' => TRUE],
        ],
      ],
    ];

    $form['frequency'] = [
      '#type' => 'select',
      '#title' => $this->t('Frequency'),
      '#description' => $this->t('The frequency the payments will be charged.'),
      '#options' => [
        '' => $this->t('Select'),
        'MONTH' => $this->t('Monthly'),
        'YEAR' => $this->t('Yearly'),
        'DAY' => $this->t('Daily'),
        'WEEK' => $this->t('Weekly'),
      ],
      '#default_value' => $this->configuration['frequency'],
      '#states' => [
        'visible' => [
          $this->getFieldInputSelecctor('dynamic_plans') => ['checked' => TRUE],
        ],
      ],
    ];

    $form['autogenerate_product'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Autogenerate product'),
      '#description' => $this->t('Check when it is needed to generate dynamically the product.'),
      '#states' => [
        'visible' => [
          $this->getFieldInputSelecctor('dynamic_plans') => ['checked' => TRUE],
          $this->getFieldInputSelecctor('product_id') => ['value' => ''],
        ],
      ],
    ];

    return $form;
  }

  /**
   * Get the selector of a specific field input (first level).
   */
  protected function getFieldInputSelecctor(string $field_name) : string {
    return sprintf(':input[name="configuration[%s][%s]"]', $this->pluginId, $field_name);
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    if ($form_state->getErrors()) {
      return;
    }
    $values = $form_state->getValue($form['#parents']);
    if (empty($values['client_id']) || empty($values['secret'])) {
      return;
    }

    if (!empty($values['autogenerate_product'])) {
      $this->logger->info(sprintf('Creating product for "%s" paymeny gateway.', $this->pluginId));
      /** @var \Drupal\commerce_paypal_subscriptions\CheckoutSdk $sdk */
      $sdk = $this->checkoutSdkFactory->get($values);
      // @todo allow modifying type and category.
      try {
        $product_create_response = $sdk->createProduct(sprintf('Drupal Commerce Paypal Subscriptions: %s', $values['display_label']), $values['display_label'], 'SERVICE', 'SOFTWARE');
        $product = json_decode((string) $product_create_response->getBody());
        $values['product_id'] = $product->id;
        $form_state->setValue($form['#parents'], $values);
        $this->messenger->addMessage('Created product for paypal subscriptions!');
      }
      catch (RequestException $exception) {
        $this->logger->error(sprintf('Error creating product: %s', $exception->getMessage()));
        $form_state->setErrorByName('autogenerate_product', 'The product could no be generated.');
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValue($form['#parents']);
    $this->configuration['dynamic_plans'] = $values['dynamic_plans'] ?? '';
    $this->configuration['default_subscription_plan'] = $values['default_subscription_plan'] ?? '';
    $this->configuration['product_id'] = $values['product_id'] ?? '';
    $this->configuration['frequency'] = $values['frequency'] ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $payment_gateway = $order->get('payment_gateway')->entity;
    if (!$payment_gateway instanceof PaymentGatewayInterface) {
      throw new PaymentGatewayException('The order does not have payment gateway!');
    }
    /** @var \Drupal\commerce_paypal_subscriptions\CheckoutSdk $checkout_sdk */
    $checkout_sdk = $this->checkoutSdkFactory->get($this->getConfiguration());

    $paypal_order_id = $request->get('orderID');
    $subscription_id = $request->get('subscriptionID');

    try {
      $subscription_response = $checkout_sdk->getSubscription($subscription_id);
      $subscription = json_decode((string) $subscription_response->getBody());

      if (empty($subscription) || $subscription->plan_id != $order->getData('paypal_subscription_plan_id')) {
        throw new PaymentGatewayException('Subscription plan id does not match with order plan ID.');
      }
    }
    catch (RequestException $exception) {
      throw new PaymentGatewayException('Subscription not found.');
    }

    // Reload order as it may have changed after saving the payment.
    $order->setData('paypal_order_id', $paypal_order_id);
    $order->setData('paypal_subscription_id', $subscription_id);

    // Create complete payment as at this moment
    // the subscription has been fully pledged.
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $payment = $payment_storage->create([
      'state' => 'completed',
      'amount' => $order->getTotalPrice(),
      'payment_gateway' => $payment_gateway->id(),
      'order_id' => $order->id(),
      'remote_id' => $subscription_id,
      'remote_state' => $subscription->status,
    ]);

    $payment->save();
  }

}
