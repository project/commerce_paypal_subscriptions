<?php

namespace Drupal\commerce_paypal_subscriptions\Controller;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\commerce_paypal\Controller\CheckoutController as OriginalCheckoutController;
use Drupal\commerce_paypal\Plugin\Commerce\PaymentGateway\CheckoutInterface;
use Drupal\commerce_paypal_subscriptions\Event\PaypalSubscriptionCreateEvent;
use Drupal\commerce_paypal_subscriptions\Event\PaypalSubscriptionEvents;
use Drupal\commerce_paypal_subscriptions\Model\Frequency;
use Drupal\commerce_paypal_subscriptions\PlanGenerator;
use Drupal\Core\Access\AccessException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Checkout.
 */
class CheckoutController extends OriginalCheckoutController {

  /**
   * Used to allow altering the paypal subscription event.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * Gets or generates Paypal plans.
   *
   * @var \Drupal\commerce_paypal_subscriptions\PlanGenerator
   */
  protected PlanGenerator $planGenerator;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);

    /** @var \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher */
    $event_dispatcher = $container->get('event_dispatcher');
    $instance->eventDispatcher = $event_dispatcher;

    /** @var \Drupal\commerce_paypal_subscriptions\PlanGenerator $plan_generator */
    $plan_generator = $container->get('commerce_paypal_subscriptions.plan_generator');
    $instance->planGenerator = $plan_generator;

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function onCreate(OrderInterface $commerce_order, PaymentGatewayInterface $commerce_payment_gateway, Request $request) {
    if (!$commerce_payment_gateway->getPlugin() instanceof CheckoutInterface) {
      throw new AccessException('Invalid payment gateway provided.');
    }

    $config = $commerce_payment_gateway->getPluginConfiguration();
    $commerce_order->set('payment_gateway', $commerce_payment_gateway);

    $dynamic_plans = $config['dynamic_plans'] ?? FALSE;
    $plan_id = '';
    if (!$dynamic_plans) {
      $plan_id = $config['default_subscription_plan'] ?? '';
    }

    $frequency_interval_unit = $config['frequency'];
    $frequency_interval_count = 1;

    $frequency = new Frequency($frequency_interval_unit, $frequency_interval_count);

    // Try to get a plan from configuration / dispatched event.
    // Other modules can use this hook to add a plan based on specific logics.
    $subscription_create_event = new PaypalSubscriptionCreateEvent($plan_id, $commerce_order, $frequency);
    $this->eventDispatcher->dispatch($subscription_create_event, PaypalSubscriptionEvents::SUBSCRIPTION_CREATE->value);
    $plan_id = $subscription_create_event->getPlanId();

    $commerce_order->setData('paypal_subscription_frequency', $frequency->toArray());

    // Get a dynamic plan in case it does not exist.
    if (empty($plan_id) && $dynamic_plans) {
      $plan_id = $this->getPaypalPlanId($commerce_order);
    }

    $commerce_order->setData('paypal_subscription_plan_id', $plan_id);
    $commerce_order->setRefreshState(OrderInterface::REFRESH_SKIP);
    $commerce_order->save();

    return new JsonResponse(['plan_id' => $commerce_order->getData('paypal_subscription_plan_id')]);
  }

  /**
   * Get a plan id for paypal.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   Order.
   */
  protected function getPaypalPlanId(OrderInterface $order) {
    $plan_id = $this->planGenerator->getOrderPlan($order);
    if (empty($plan_id)) {
      $plan_id = $this->planGenerator->createOrderPlan($order);
    }
    return $plan_id;
  }

}
