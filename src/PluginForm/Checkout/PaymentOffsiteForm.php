<?php

namespace Drupal\commerce_paypal_subscriptions\PluginForm\Checkout;

use Drupal\commerce_paypal\PluginForm\Checkout\PaymentOffsiteForm as OriginalPaymentOffsiteForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form to make the Paypal subscriptions offsite.
 */
class PaymentOffsiteForm extends OriginalPaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @var \Drupal\commerce_paypal_subscriptions\SmartPaymentsButtonsBuilder $smart_payment_buttons_builder */
    $smart_payment_buttons_builder = $container->get('commerce_paypal_subscriptions.smart_payment_buttons_builder');
    return new PaymentOffsiteForm($smart_payment_buttons_builder);
  }

}
