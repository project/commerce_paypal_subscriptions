<?php

namespace Drupal\commerce_paypal_subscriptions;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreInterface;

/**
 * Generates plans used in commerce orders.
 */
class PlanGenerator {

  /**
   * Instantiates the SDK used to create plans.
   *
   * @var CheckoutSdkFactory
   */
  protected CheckoutSdkFactory $checkoutSdkFactory;

  /**
   * Stores a plan for each different payment gateway / price.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected KeyValueStoreInterface $keyValueStore;

  /**
   * Constructs the generator.
   *
   * @param CheckoutSdkFactory $checkout_sdk_factory
   *   CHeckout SDK Factory service.
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $keyValueFactory
   *   Key Value Factory service.
   */
  public function __construct(
    CheckoutSdkFactory $checkout_sdk_factory,
    KeyValueFactoryInterface $keyValueFactory,
  ) {
    $this->checkoutSdkFactory = $checkout_sdk_factory;
    $this->keyValueStore = $keyValueFactory->get('commerce_paypal_subscriptions.plans');
  }

  /**
   * Get the plan for a specific order.
   */
  public function getOrderPlan(OrderInterface $order) {
    $key = $this->getOrderPlanKey($order);
    return $this->keyValueStore->get($key);
  }

  /**
   * Get the key that belongs to a specific order plan.
   *
   * @return string
   *   Order plan key.
   */
  protected function getOrderPlanKey(OrderInterface $order) {
    $payment_gateway = $this->getPaymentGateway($order);
    $frequency_key = (string) $order->getData('commerce_paypal_subscriptions_frequency');
    $price_key = str_replace(' ', '-', (string) $order->getTotalPrice());
    return sprintf('%s-%s-%s', $payment_gateway->id(), $price_key, $frequency_key);
  }

  /**
   * Creates a plan for a specific order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   Order.
   */
  public function createOrderPlan(OrderInterface $order) {
    $payment_gateway = $this->getPaymentGateway($order);
    $payment_gateway_configuration = $payment_gateway->getPluginConfiguration();
    /** @var CheckoutSdk $checkout_sdk */
    $checkout_sdk = $this->checkoutSdkFactory->get($payment_gateway_configuration);
    $product_id = $payment_gateway_configuration['product_id'];
    $plan_name = $payment_gateway->label();
    $name = sprintf('%s: %s', $plan_name, $order->getTotalPrice());

    $description = sprintf('Plan created through drupal commerce paypal module. Payment gateway: %s. Price: %s', $payment_gateway->id(), $order->getTotalPrice());

    /** @var \Drupal\commerce_price\Price $total_price */
    $total_price = $order->getTotalPrice();
    $billing_cycles = [];
    $billing_cycles[] = [
      'frequency' => $order->getData('paypal_subscription_frequency'),
      'tenure_type' => 'REGULAR',
      'total_cycles' => 0,
      'sequence' => '1',
      'pricing_scheme' => [
        'fixed_price' => [
          'value' => round((float) $total_price->getNumber(), 2, PHP_ROUND_HALF_DOWN),
          'currency_code' => $total_price->getCurrencyCode(),
        ],
      ],
    ];

    $plan_create_response = $checkout_sdk->createSubscriptionPlan($product_id, $name, $description, $billing_cycles);
    $plan = json_decode((string) $plan_create_response->getBody());
    $key = $this->getOrderPlanKey($order);
    $this->keyValueStore->set($key, $plan->id);
    return $plan->id;
  }

  /**
   * Gets the payment gateway of an order.
   *
   * If it is not present, throws an exception
   * as it should be.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   Order.
   */
  protected function getPaymentGateway(OrderInterface $order) : PaymentGatewayInterface {
    $payment_gateway = $order->get('payment_gateway')->entity;
    if (!$payment_gateway instanceof PaymentGatewayInterface) {
      throw new \InvalidArgumentException('Order does not have payment gateway.');
    }

    return $payment_gateway;
  }

}
