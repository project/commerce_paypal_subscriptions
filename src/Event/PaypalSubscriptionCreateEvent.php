<?php

namespace Drupal\commerce_paypal_subscriptions\Event;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_paypal_subscriptions\Model\Frequency;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event dispatched when a subscription is being created.
 */
class PaypalSubscriptionCreateEvent extends Event {

  /**
   * Constructs the event.
   *
   * @param string $planId
   *   Subscription plan ID.
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   Order.
   * @param \Drupal\commerce_paypal_subscriptions\Model\Frequency $frequency
   *   Frequency.
   */
  public function __construct(
    protected string $planId,
    protected OrderInterface $order,
    protected Frequency $frequency,
  ) {}

  /**
   * Gets the subscription plan ID.
   */
  public function getPlanId() : string {
    return $this->planId;
  }

  /**
   * Sets the subscription plan Id.
   *
   * @param string $plan_id
   *   Plan ID.
   */
  public function setPlanId(string $plan_id) {
    $this->planId = $plan_id;
  }

  /**
   * Gets the order.
   */
  public function getOrder() : OrderInterface {
    return $this->order;
  }

  /**
   * Gets the frequency.
   */
  public function getFrequency() : Frequency {
    return $this->frequency;
  }

}
