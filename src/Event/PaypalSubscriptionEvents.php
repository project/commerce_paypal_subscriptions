<?php

namespace Drupal\commerce_paypal_subscriptions\Event;

/**
 * Events dispatched by this module.
 */
enum PaypalSubscriptionEvents : string {

  /* Executed before executing createSubscription action from paypal SDK. */
  case SUBSCRIPTION_CREATE = 'commerce_paypal_subscriptions.create';

}
