<?php

namespace Drupal\commerce_paypal_subscriptions;

use Drupal\commerce_paypal\CheckoutSdkFactory as OriginalCheckoutSdkFactory;

/**
 * Allows instancing the checkout SDK.
 */
class CheckoutSdkFactory extends OriginalCheckoutSdkFactory {

  /**
   * {@inheritdoc}
   *
   * @return \Drupal\commerce_paypal\CheckoutSdkInterface
   *   The PayPal Checkout SDK.
   */
  public function get(array $configuration) {
    $client_id = $configuration['client_id'];
    if (!isset($this->instances[$client_id])) {
      $client = $this->getClient($configuration);
      $this->instances[$client_id] = new CheckoutSdk($client, $this->adjustmentTransformer, $this->eventDispatcher, $this->moduleHandler, $this->time, $configuration, $this->rounder);
    }

    return $this->instances[$client_id];
  }

}
