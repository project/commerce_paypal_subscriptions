<?php

namespace Drupal\commerce_paypal_subscriptions\Model;

/**
 * Paypal subscriptions frequency object.
 *
 * @see https://developer.paypal.com/docs/api/subscriptions/v1/#plans_create!path=billing_cycles/frequency&t=request
 */
class Frequency implements \Stringable {

  /**
   * Constructs the paypal frequency objecct.
   *
   * @param string $intervalUnit
   *   Interval unit.
   * @param int $intervalCount
   *   Interval count.
   */
  public function __construct(
    protected string $intervalUnit,
    protected int $intervalCount,
  ) {}

  /**
   * Sets the interval unit.
   *
   * @param string $interval_unit
   *   Interval unit.
   */
  public function setIntervalUnit(string $interval_unit) : void {
    $this->intervalUnit = $interval_unit;
  }

  /**
   * Gets the interval unit.
   */
  public function getIntervalUnit() : string {
    return $this->intervalUnit;
  }

  /**
   * Sets the interval count.
   *
   * @param int $interval_count
   *   Inteval count.
   */
  public function setIntervalCount(int $interval_count) : void {
    $this->intervalCount = $interval_count;
  }

  /**
   * Gets the interval count.
   */
  public function getIntervalCount() : int {
    return $this->intervalCount;
  }

  /**
   * Converts the frequency into an array.
   *
   * @return array
   *   Same structure as documented paypal subscription frequency.
   */
  public function toArray() {
    return [
      'interval_unit' => $this->intervalUnit,
      'interval_count' => $this->intervalCount,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function __toString() {
    return sprintf('%s-%s', $this->intervalUnit, $this->intervalCount);
  }

}
