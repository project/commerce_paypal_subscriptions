(function ($, Drupal) {
  'use strict';

  Drupal.paypalCheckout = {
    initialized: false,
    makeCall(url, settings) {
      settings = settings || {};
      const ajaxSettings = {
        dataType: 'json',
        url: url
      };
      $.extend(ajaxSettings, settings);
      return $.ajax(ajaxSettings);
    },
    renderButtons(settings) {
      paypal.Buttons({
        createSubscription: (data, actions) => {
          return Drupal.paypalCheckout.makeCall(settings.onCreateUrl, {
            type: 'POST',
          }).then((data) => {
            return actions.subscription.create(data);
          }, (data) => {
            const messages = new Drupal.Message();
            const options = {
              type: 'error'
            };
            messages.clear();
            messages.add(data.responseJSON.message, options);
            if (data.status === 403) {
              location.reload();
            }
          });
        },
        onApprove: (data) => {
          console.log(data, 'DATA');
          Drupal.paypalCheckout.addLoader();
          return Drupal.paypalCheckout.makeCall(settings.onApproveUrl, {
            method: 'POST',
            data: data
          }).then((data) => {
            if (data.hasOwnProperty('redirectUrl')) {
              window.location.assign(data.redirectUrl);
            }
          }, (data) => {
            const messages = new Drupal.Message();
            const options = {
              type: 'error'
            };
            messages.clear();
            messages.add(data.responseJSON.message, options);
            if (data.status === 403) {
              location.reload();
            }
          });
        },
        onCancel: function (data) {
          if (settings.hasOwnProperty('onCancelUrl')) {
            window.location.assign(settings.onCancelUrl);
          }
        },
        style: settings['style']
      }).render('#' + settings['elementId']);
    },
    initialize(settings) {
      if (!this.initialized) {
        // Ensure we initialize the script only once.
        this.initialized = true;
        let script = document.createElement('script');
        script.src = settings.src;
        script.type = 'text/javascript';
        script.setAttribute('data-partner-attribution-id', 'CommerceGuys_Cart_SPB');
        document.getElementsByTagName('head')[0].appendChild(script);
      }
      const waitForSdk = (settings) => {
        if (typeof paypal !== 'undefined') {
          Drupal.paypalCheckout.renderButtons(settings);
        }
        else {
          setTimeout(() => {
            waitForSdk(settings)
          }, 100);
        }
      };
      waitForSdk(settings);
    },
    addLoader() {
      const $background = $('<div class="paypal-background-overlay"></div>');
      const $loader = $('<div class="paypal-background-overlay-loader"></div>');
      $background.append($loader);
      $('body').append($background);
    }
  };

  Drupal.behaviors.commercePaypalCheckout = {
    attach(context, settings) {
      for (let index in settings.paypalSubscriptions) {
        once('paypal-checkout-init', '#' + settings.paypalSubscriptions[index]['elementId']).forEach(() => {
          Drupal.paypalCheckout.initialize(settings.paypalSubscriptions[index]);
        });
      }
    }
  };

})(jQuery, Drupal);
