# Introduction

*WARNING*: This module is not yet stable. Use it in production under your own responsibility

Enables commerce order to be purchased through paypal subscriptions. This module may be helpful to you in case you want recurring payments in your site.

The paypal products and plans can be generated dynamically. Currently, the price of the plans is based on the commerce order total price.

This module uses the modern Paypal API. In case you need to use NVP/SOAP API, please use Paypal subscriptions

For more information about Paypal subscriptions please checkout out the official documentation

# Post-Installation

After installing the module, it is possible to add a payment gateway with 'Paypal checkout subscriptions' payment method. It requires Paypal client ID and client secret.

To finish configuring the checkout it is needed to have a product and a plan in your Paypal account. In case you want them to be generated dynamically, select 'Dynamic plans' and check 'Autogenerate product'. Otherwise, you can fill the 'Default subscription plan' with a subscription plan that must have been previously created.
